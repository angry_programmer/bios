CC=g++-5
TARGET=biosensor

SOURCEDIR=./src
SOURCE = $(SOURCEDIR)/main.c $(SOURCEDIR)/modelling.c $(SOURCEDIR)/parameters.c $(SOURCEDIR)/results.c $(SOURCEDIR)/types.c
# SOURCE = $(SOURCEDIR)/config.c

## End sources definition
# INCLUDE =-I/usr/local/include  
# INCLUDE = -I.$(SOURCEDIR)/../inc
## end more includes

CCFLAGS =-std=c++0x -Wall -O2 -funroll-loops -mtune=native
# LIBS=-L/usr/local/lib -lboost_serialization-mt

VPATH=$(SOURCEDIR)
OBJ=$(join $(addsuffix ../obj/, $(dir $(SOURCE))), $(notdir $(SOURCE:.c=.o))) 

## Fix dependency destination to be ../.dep relative to the src dir
DEPENDS=$(join $(addsuffix ../.dep/, $(dir $(SOURCE))), $(notdir $(SOURCE:.c=.d)))

## Default rule executed
all: $(TARGET)
	@true

## Clean Rule
clean:
	@-rm -f $(TARGET) $(OBJ) $(DEPENDS)


## Rule for making the actual target
$(TARGET): $(OBJ)
	@echo "============="
	@echo "Linking the target $@"
	@echo "============="
	@$(CC) $(CFLAGS) -o $@ $^ $(LIBS)
	@echo -- Link finished --

## Generic compilation rule
%.o : %.c
	@mkdir -p $(dir $@)
	@echo "============="
	@echo "Compiling $<"
	@$(CC) $(CFLAGS) -c $< -o $@


# Rules for object files from c files
# Object file for each file is put in obj directory
# one level up from the actual source directory.
../obj/%.o : %.c
	@mkdir -p $(dir $@)
	@echo "============="
	@echo "Compiling $<"
	@$(CC) $(CFLAGS) -c $< -o $@

# Rule for "other directory"  You will need one per "other" dir
$(SOURCEDIR)/../obj/%.o : %.c
	@mkdir -p $(dir $@)
	@echo "============="
	@echo "Compiling $<"
	@$(CC) $(CFLAGS) ${INCLUDE} -c $< -o $@

# Make dependancy rules
../.dep/%.d: %.c
	@mkdir -p $(dir $@)
	@echo "============="
	@echo Building dependencies file for $*.o
	@$(SHELL) -ec '$(CC) -M $(CFLAGS) $< | sed "s^$*.o^../obj/$*.o^" > $@'

## Dependency rule for "other" directory
$(SOURCEDIR)/../.dep/%.d: %.c
	@mkdir -p $(dir $@)
	@echo "============="
	@echo Building dependencies file for $*.o
	@$(SHELL) -ec '$(CC) -M $(CFLAGS) $< | sed "s^$*.o^$(SOURCEDIR)/../obj/$*.o^" > $@'

## Include the dependency files
-include $(DEPENDS)