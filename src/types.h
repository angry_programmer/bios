//
//  types.h
//  MBD-II
//
//  Created by Gintaras on 15/10/14.
//  Copyright (c) 2014 Gintaras Dreizas. All rights reserved.
//

#ifndef MBD_II_types_h
#define MBD_II_types_h

#include <sys/types.h>
#include <math.h>

#ifndef max
#define max( a, b ) ( ((a) > (b)) ? (a) : (b) )
#endif

#ifndef min
#define min( a, b ) ( ((a) < (b)) ? (a) : (b) )
#endif

static const size_t VEC_SIZE = 4;

typedef double Real;
typedef Real Vec4[VEC_SIZE];
#ifndef __cplusplus
typedef char bool;
#endif
typedef char err;

#define true 1
#define false 0

typedef Real(*function)(
                        const Real t,
                        const Vec4 v
                        );
typedef Real(*defectation)(
                           const Real t0,
                           const Real t,
                           const Real v,
                           const Real K,
                           const Real M
                           );

//#define LOG_INNER_FITTING

Real sqr(const Real x);

Real Vec4_len(
              const Vec4 v
              );

void Vec4_init(
               Vec4 r,
               Real v
               );

void Vec4_cpy(
              const Vec4 from,
              Vec4 to
              );

void Vec4_sum(
              const Vec4 a,
              const Vec4 b,
              Vec4 r
              );

void Vec4_mul(
              const Vec4 a,
              const Vec4 b,
              Vec4 r
              );

Real angle_between(
                   const Real x1,
                   const Real x2,
                   const Real y1,
                   const Real y2
                   );

void arr_cpy(
             Real **dst,
             Real **src,
             size_t len
             );

Real find_max(
              const Real *a,
              const size_t n
              );

Real least_squares(
                   const Real* a,
                   const Real* b,
                   const size_t n
                   );

extern const err ERR_NULL;
extern const err ERR_UNABLE_TO_OPEN_FILE;

void err_message(
                 const err e,
                 char* m
                 );

unsigned long long getTimestamp();

#endif
