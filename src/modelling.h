//
//  modelling.h
//  MBD-II
//
//  Created by Gintaras on 04/01/15.
//  Copyright (c) 2015 Gintaras Dreizas. All rights reserved.
//

#ifndef MBD_II_modelling_h
#define MBD_II_modelling_h

#include <assert.h>
#include <time.h>
#include "parameters.h"
#include "results.h"

struct ModellingParams {
    Real *s_prev;
    Real *p_prev;
    Real *e_prev;
    
    Real *s_array;
    Real *p_array;
    Real *e_array;
    
    Real *s_new;
    Real *p_new;
    Real *e_new;
    
    Real tau_prev;
    Real tau;
    Real tau_next;
    
    Real t;
    
    Real max_dS_dt_prev;
    Real max_dS_dt_curr;
    
    long k;
    Real t_slopeMAX;
    
    Real I_prev;
    Real I_prev2;
    Real I;
    
    Real dI_prev;
    Real dI_prev2;
    Real dI;
    
    Real dI_max;
    
    FILE *fo;
    FILE *fo2;
    
    Real last_i;
    Real last_di;
    
    char current_path[256];
};

void run_modelling(
                   const char* base_path,
                   const Real value,
                   const int c,
                   Real result[2][2]
                   );

#endif
