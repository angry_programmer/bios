//
//  Header.h
//  MBD
//
//  Created by Gintaras Dreizas on 03/04/14.
//  Copyright (c) 2014 MIF. All rights reserved.
//

#ifndef MBD_Header_h
#define MBD_Header_h

//#define DEBUG_LOG

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <float.h>
#include <math.h>
#include "parameters.h"

static int product_diffuses_through_the_outer_membrane = 0;

static Real tau_min;
static Real q_max = 1.1;
static const Real F = 96485.3365;

Real h() { return (params.Dm1 + params.Dm2 + params.De) / (params.N + 0.0); }

/**
 *
 */
Real absolute(Real v)
{
    return max(v, -v);
}

Real Ds(Real x)
{
    if (0.0 < x && x < params.Dm1)
    {
        return params.DSm1;
    }
    else if (params.Dm1 <= x && x <= (params.Dm1 + params.De))
    {
        return params.DSe;
    }
    else if ((params.Dm1 + params.De) < x && x < (params.Dm1 + params.De + params.Dm2))
    {
        return params.DSm2;
    }
    return 0.0;
}

Real Dp(Real x)
{
    if (x == 0.0)
    {
        if (params.Dm1 != 0.0)
        {
            return params.DPm1;
        }
        else if (params.Dm1 == 0.0)
        {
            return params.DPe;
        }
    }
    
    if (x > 0.0 && x < params.Dm1)
    {
        return params.DPm1;
    }
    else if (x >= params.Dm1 && x <= (params.Dm1 + params.De))
    {
        return params.DPe;
    }
    else if (x > (params.Dm1 + params.De) && x < (params.Dm1 + params.De + params.Dm2))
    {
        return params.DPm2;
    }
    return 0.0;
}

Real alpha(const Real x)
{
    if (x == 0.0)
    {
        if (params.Dm1 != 0.0)
        {
            return 0.0;
        }
        else if (params.Dm1 == 0.0)
        {
            return 1.0;
        }
    }
    
    if (x > 0.0 && x < params.Dm1)
    {
        return 0.0;
    }
    else if (x >= params.Dm1 && x <= (params.Dm1 + params.De))
    {
        return 1.0;
    }
    else if (x > (params.Dm1 + params.De) && x < (params.Dm1 + params.De + params.Dm2))
    {
        return 0.0;
    }
    
    return 0.0;
}

Real S(size_t i);
Real P(size_t i);
Real E(size_t i);

Real x(size_t i, int next)
{
    if (i == 0 && next < 0)
        return 0.0;
    return ((i * h()) + ((i + next) * h())) / 2.0;
}

//Real tau(size_t k);
//Real t(size_t k);

Real S_a(size_t i)
{
    return Ds(x(i, -1)) / (h() * h());
}

Real S_b(size_t i)
{
    return Ds(x(i, 1)) / (h() * h());
}

Real P_a(size_t i)
{
    return Dp(x(i, -1)) / (h() * h());
}

Real P_b(size_t i)
{
    return Dp(x(i, 1)) / (h() * h());
}

Real S_c(Real e_old, size_t i);
Real S_f(Real e_old, size_t i);
Real P_c(Real e_old, size_t i);
Real P_f(Real e_old, size_t i);
void E_(Real *e_new, Real e_old, Real s_old, Real s_new, Real p_old, Real p_new, size_t i);

void solve_system();
void solve_system_S(Real* e_old, Real* new_array);
void solve_system_P(Real* e_old, Real* new_array);
void tdma(Real* a, Real* b, Real* c, Real *f, Real r0, Real rn, Real* result);
void tdma2(Real* a, Real* b, Real* c, Real *f, Real* result);

Real calc_I(Real *p);
Real calc_dI(const Real v, const Real *x, const Real *u);

void printToCSV();

#endif
