//
//  main.c
//  p1_weighting
//
//  Created by Gintaras on 04/01/15.
//  Copyright (c) 2015 Gintaras Dreizas. All rights reserved.
//

#define MAIN_FILE
#include "parameters.h"
#include "modelling.h"
#include <stdlib.h>
#include <float.h>

static const char BASE_PATH[32] = "results/weighting";
static const int N_VARS = 11;
static const char * VARS[] = {
    "dm1",
    "dm2",
    "de",
    "DPm1",
    "DPm2",
    "DPe",
    "DSm1",
    "DSm2",
    "DSe",
    "k+1",
    "k-1",
    // "k+3",
};

size_t iterations_count;
Real percent_per_iteration;

void runModellingForVar(size_t varIndex);

int main(int argc, const char * argv[]) {
    
    if (argc < 2) {
        fprintf(stderr, "Nenurodytas pradinis skaicius.");
        exit(EXIT_FAILURE);
    }
    
    iterations_count = atoi(argv[1]);
    if (iterations_count > 1000) {
        fprintf(stderr, "Abejoju ar norite taip ilgai laukti modeliavimo rezultatu.");
        exit(EXIT_FAILURE);
    }
    percent_per_iteration = 1.0 / (Real) iterations_count;
    
    if (argc < 3) {
        fprintf(stderr, "Nenurodytas parametru ribas apibrezintis duomenu failas.");
        exit(EXIT_FAILURE);
    }
    load_bounds(argv[2]);
    set_var_percent("k+3", 0.038);
    Real result[2][2];
    char controlPath[64];
    sprintf(controlPath, "%s/kontrole", BASE_PATH);
    run_modelling(controlPath, 0.5, 0, result);

    char filename[48];
    sprintf(filename, "%s/raw.csv", controlPath);
    FILE *inner = fopen(filename, "w");
    fprintf(inner, "ndiff_t;ndiff_I;tdiff_t;tdiff_I\n");
    fprintf(inner, "%.6lf;%.6lf;%.6lf;%.6lf\n", result[0][0], result[0][1], result[1][0], result[1][1]);
    fclose(inner);
    
    for (size_t varIndex = 0; varIndex < N_VARS; varIndex++) {
        printf("%lu of %d\n", (varIndex + 1), N_VARS);
        runModellingForVar(varIndex);
    }
    
    return EXIT_SUCCESS;
}

void runModellingForVar(size_t varIndex) {
    
    const char * param = VARS[varIndex];
    
    char rpath[64];
    sprintf(rpath, "%s/%s", BASE_PATH, param);
    {
            char shell_cmd[256];
            sprintf(shell_cmd, "mkdir -p %s", rpath);
            system(shell_cmd);
        }
    Real percent = 0.0;
    int counter = 0;
    char filename[32];
    sprintf(filename, "%s/raw.csv", rpath);
    FILE *inner = fopen(filename, "w");
    if (inner == NULL) {
        printf("inner = NULL\n");
        exit(EXIT_FAILURE);
    }
    fprintf(inner, "pct;ndiff_t;ndiff_I;tdiff_t;tdiff_I\n");

    do {
        Real result[2][2];
        set_var_percent("k+3", 0.038);
        Real value = set_var_percent(param, percent);
        run_modelling(rpath, value, counter, result);
        fprintf(inner, "%.6lf;%.6lf;%.6lf;%.6lf;%.6lf\n", percent, result[0][0], result[0][1], result[1][0], result[1][1]);
        counter++;
        percent += percent_per_iteration;
        if (percent == 0.5) {
            percent += percent_per_iteration;
        }
    } while (1.0 >= percent);
    fclose(inner);
    set_var_percent(param, 0.5);
    
    // daliniti is vidurines reiksmes.
//    fprintf(out, "%s;%.6lf;%.6lf;%.6lf;%.6lf\n",
//            param,
//            finalResult[0][0] / (Real) (counter - 1),
//            finalResult[0][1] / (Real) (counter - 1),
//            finalResult[1][0] / (Real) (counter - 1),
//            finalResult[1][1] / (Real) (counter - 1)
//            );
    
}
