//
//  parameters.c
//  MBD-II
//
//  Created by Gintaras on 04/01/15.
//  Copyright (c) 2015 Gintaras Dreizas. All rights reserved.
//

#include "parameters.h"
#include <string.h>
#include <assert.h>

static const char* _N = "N";
static const char* _T = "T";
static const char* _tau_max = "tau_max";
static const char* _dm1 = "dm1";
static const char* _dm2 = "dm2";
static const char* _de = "de";
static const char* _dpm1 = "DPm1";
static const char* _dpm2 = "DPm2";
static const char* _dpe = "DPe";
static const char* _dsm1 = "DSm1";
static const char* _dsm2 = "DSm2";
static const char* _dse = "DSe";
static const char* _c1 = "C1";
static const char* _c2 = "C2";
static const char* _kp1 = "k+1";
static const char* _km1 = "k-1";
static const char* _kp3 = "k+3";
static const char* _km3 = "k-3";
static const char* _ne = "ne";
static const char* _vmax = "Vmax";
static const char* _km = "KM";
static const char* _s0 = "S0";
static const char* _p0 = "P0";
static const char* _e0 = "E0";
static const char* _Selectrode = "S_electrode";

/**
 * 0 - apatine riba.
 * 1 - virsutine riba.
 */
static struct Parameters bounds[2];

void set_initial_data()
{
    params.N = 8.0e2;
    params.tau_max = 5.0e-3;
    params.T = 1.0e5;
    params.C1 = 0.0;
    params.C2 = 0.0;
    params.S0 = 7.0e-2;
    params.E0 = 1.0e-2;
    params.k_plus_1 = 2307.6923076923076923076923;
    params.k_minus_1 = 230.76923076923076923076923;
    params.k_plus_3 = 3.0e2;
    params.k_minus_3 = 3.0e2;
    params.ne = 1.0;
}

bool is_strings_equal(const char* s1, const char* s2) { return (strcmp(s1, s2) == 0); }

Real set_var_percent(
             const char* var,
             const Real pct
             )
{
    if (is_strings_equal(_dm1, var)) { params.Dm1 = (bounds[1].Dm1 - bounds[0].Dm1) * pct + bounds[0].Dm1; return params.Dm1; }
    if (is_strings_equal(_dm2, var)) { params.Dm2 = (bounds[1].Dm2 - bounds[0].Dm2) * pct + bounds[0].Dm2; return params.Dm2; }
    if (is_strings_equal(_de, var)) { params.De = (bounds[1].De - bounds[0].De) * pct + bounds[0].De; return params.De; }
    if (is_strings_equal(_dpm1, var)) { params.DPm1 = (bounds[1].DPm1 - bounds[0].DPm1) * pct + bounds[0].DPm1; return params.DPm1; }
    if (is_strings_equal(_dpm2, var)) { params.DPm2 = (bounds[1].DPm2 - bounds[0].DPm2) * pct + bounds[0].DPm2; return params.DPm2; }
    if (is_strings_equal(_dpe, var)) { params.DPe = (bounds[1].DPe - bounds[0].DPe) * pct + bounds[0].DPe; return params.DPe; }
    if (is_strings_equal(_dsm1, var)) { params.DSm1 = (bounds[1].DSm1 - bounds[0].DSm1) * pct + bounds[0].DSm1; return params.DSm1; }
    if (is_strings_equal(_dsm2, var)) { params.DSm2 = (bounds[1].DSm2 - bounds[0].DSm2) * pct + bounds[0].DSm2; return params.DSm2; }
    if (is_strings_equal(_dse, var)) { params.DSe = (bounds[1].DSe - bounds[0].DSe) * pct + bounds[0].DSe; return params.DSe; }
    if (is_strings_equal(_kp1, var)) { params.k_plus_1 = (bounds[1].k_plus_1 - bounds[0].k_plus_1) * pct + bounds[0].k_plus_1; return params.k_plus_1; }
    if (is_strings_equal(_kp3, var)) { params.k_plus_3 = (bounds[1].k_plus_3 - bounds[0].k_plus_3) * pct + bounds[0].k_plus_3; return params.k_plus_3; }
    if (is_strings_equal(_km1, var)) { params.k_minus_1 = (bounds[1].k_minus_1 - bounds[0].k_minus_1) * pct + bounds[0].k_minus_1; return params.k_minus_1; }
    return 0.0;
}

void set_var(
             const char* var,
             const Real value,
             struct Parameters *
             
             p
             )
{
    if (is_strings_equal(_N, var)) { p->N = (int) value; return; }
    if (is_strings_equal(_T, var)) { p->T = value; return; }
    if (is_strings_equal(_tau_max, var)) { p->tau_max = value; return; }
    if (is_strings_equal(_dm1, var)) { p->Dm1 = value; return; }
    if (is_strings_equal(_dm2, var)) { p->Dm2 = value; return; }
    if (is_strings_equal(_de, var)) { p->De = value; return; }
    if (is_strings_equal(_dm2, var)) { p->Dm2 = value; return; }
    if (is_strings_equal(_dpm1, var)) { p->DPm1 = value; return; }
    if (is_strings_equal(_dpm2, var)) { p->DPm2 = value; return; }
    if (is_strings_equal(_dpe, var)) { p->DPe = value; return; }
    if (is_strings_equal(_dsm1, var)) { p->DSm1 = value; return; }
    if (is_strings_equal(_dsm2, var)) { p->DSm2 = value; return; }
    if (is_strings_equal(_dse, var)) { p->DSe = value; return; }
    if (is_strings_equal(_c1, var)) { p->C1 = value; return; }
    if (is_strings_equal(_c2, var)) { p->C2 = value; return; }
    if (is_strings_equal(_kp1, var)) { p->k_plus_1 = value; return; }
    if (is_strings_equal(_kp3, var)) { p->k_plus_3 = value; return; }
    if (is_strings_equal(_km1, var)) { p->k_minus_1 = value; return; }
    if (is_strings_equal(_km3, var)) { p->k_minus_3 = value; return; }
    if (is_strings_equal(_ne, var)) { p->ne = value; return; }
    if (is_strings_equal(_vmax, var)) { p->Vmax = value; return; }
    if (is_strings_equal(_km, var)) { p->KM = value; return; }
    if (is_strings_equal(_s0, var)) { p->S0 = value; return; }
    if (is_strings_equal(_p0, var)) { p->P0 = value; return; }
    if (is_strings_equal(_e0, var)) { p->E0 = value; return; }
    if (is_strings_equal(_Selectrode, var)) { p->Selectrode = value; return; }
}

void load_bounds(
                 const char* filename
                 )
{
    set_initial_data();
    FILE* file = fopen(filename, "rt");
    char line[256];
    char var[32];
    Real value[2];
    while (fgets(line, 256, file)) {
        sscanf(line, "%s %lf %lf", var, &value[0], &value[1]);
        if (value[0] > value[1]) {
            fprintf(stderr, "Ribos '%s' apatinė reikšmė [%lf] yra didesnė nei viršutinė [%lf]! Patikrinkite duomenų bylą.", var, value[0], value[1]);
            assert(false);
        }
        set_var(var, value[0], &bounds[0]);
        set_var(var, value[1], &bounds[1]);
        set_var_percent(var, 0.5);
    }
    fclose(file);
}
