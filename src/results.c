//
//  results.c
//  MBD-II
//
//  Created by Gintaras on 04/01/15.
//  Copyright (c) 2015 Gintaras Dreizas. All rights reserved.
//

#include "results.h"
#include <stdlib.h>

void init_results(
                  const size_t count
)
{
    if (results == NULL) {
        results = (struct Results **) malloc(sizeof(struct Results*) * count);
        for (size_t index = 0; index < count + 1; index++) {
            results[index] = (struct Results *) malloc(sizeof(struct Results) * 2);
        }
    }
}