//
//  results.h
//  MBD-II
//
//  Created by Gintaras on 04/01/15.
//  Copyright (c) 2015 Gintaras Dreizas. All rights reserved.
//

#ifndef __MBD_II__results__
#define __MBD_II__results__

#include "types.h"

struct Results {
    Real tSlopeMaxValue;
    Real IValue;
};

#ifndef SHAREFILE2_INCLUDED
#define SHAREFILE2_INCLUDED
#ifdef  MAIN_FILE
struct Results **results;
#else
extern struct Results **results;
#endif
#endif

void init_results(
                const size_t count
);

#endif /* defined(__MBD_II__results__) */
