//
//  parameters.h
//  MBD-II
//
//  Created by Gintaras on 04/01/15.
//  Copyright (c) 2015 Gintaras Dreizas. All rights reserved.
//

#ifndef __MBD_II__parameters__
#define __MBD_II__parameters__

#include <stdio.h>
#include "types.h"

struct Parameters {
    Real DPm1;
    Real DPe;
    Real DPm2;
    
    Real DSm1;
    Real DSe;
    Real DSm2;
    
    Real Dm1;
    Real De;
    Real Dm2;
    
    Real k_plus_1;
    Real k_plus_3;
    Real k_minus_1;
    Real k_minus_3;
    
    Real C1;
    Real C2;
    
    Real S0;
    Real P0;
    Real E0;
    
    Real T;
    Real tau_max;
    
    Real ne;
    Real Vmax;
    Real KM;
    
    Real Selectrode;
    
    int N;
};

#ifndef SHAREFILE_INCLUDED
#define SHAREFILE_INCLUDED
#ifdef  MAIN_FILE
struct Parameters params;
#else
extern struct Parameters params;
#endif
#endif

void load_bounds(
                 const char* filename
                 );

Real set_var_percent(
                     const char* var,
                     const Real pct
                     );

#endif /* defined(__MBD_II__parameters__) */
